from random import shuffle

import player

class Deck(object):
    def __init__(self):
        # Total cards in deck.
        self.cards = [player.Card] 

        # Array of cards currently in play.
        self.active = [player.Card]

        # Populate the deck with every combination of suit/values
        for suit in range(0, 4):
            for value in range(1, 15):
                self.cards.append(player.Card(suit, value)) 

    def shuffle(self):
        ''' Shuffles the position of the cards in the deck. '''

        self.cards.extend(self.active)
        self.active = []
        shuffle(self.cards)

    def distribute(self, players: [player.Player]):
        ''' Hands out cards to each player. '''
		# Return false if not enough cards remaining
        if (number_of_players * 2 > len(self.cards)):
            return False

        for i in range(0, number_of_cards):
            inPlay.append(self.cards.pop(0))

        self.inPlay.extend(inplay)
        return inplay

    def deal(self) -> player.Card:
        ''' Deals a single card from the deck
        When dealing a card
        '''
        return card

	# Return amount of cards remaining in deck
    def cards_left(self) -> int:
        return len(self.cards)