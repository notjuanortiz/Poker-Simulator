import sys
import random
import poker

from player import Player
from player import Card

number_of_players = 4

players = [Player("Tim"), Player("Joe"), Player("Josie"), Player("Tiffany")]

print(" ---- Players ---- ")

for player in players:
    print(player.username)
print(" ----------------- ")
poker = poker.Poker(number_of_players)
if not poker:
	sys.exit("ERROR: Invalid number of players. Must be between 2 and 10")

# Shuffle the deck
print("Shuffling...")
poker.deck.shuffle()

# Deal cards to players
print("Dealing cards...")
players_hands = poker.deal()
if not players_hand:
	 sys.exit("ERROR: Not enough cards to distribute.")

# Show each players hand with probability to win
print(" ---- Hands ---- ")
print(" --------------- ")
for hand in players_hands:
    text = "Player - "

    for card in hand: # Display each card
        text += str(card) + ", "

    text += " : " + calculate(hand) + "%"
    printLn(text)

print("-----------------------")